package com.mycompany.mymemo.fragments;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mycompany.mymemo.R;
import com.mycompany.mymemo.app.AppController;
import com.mycompany.mymemo.clickevents.HomeScreenMethods;
import com.mycompany.mymemo.constants.ClassNames;
import com.squareup.picasso.Picasso;

public class ShowMemoFragment extends Fragment {

    private int userId = AppController.preferences.getPreference_Int("pref_userid", -1);

    private TextView txtMemoText;
    private ImageView imgMemoImage;
    private Button btnEditMemo;
    private TextView commonTitle, commonActionButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_memo, container, false);
        try {
            txtMemoText = (TextView) view.findViewById(R.id.txt_show_memo_text);
            imgMemoImage = (ImageView) view.findViewById(R.id.img_show_memo);
            commonTitle = (TextView) getActivity().findViewById(R.id.action_bar_title);
            commonActionButton = (TextView) getActivity().findViewById(R.id.action_bar_button);
            btnEditMemo = (Button) view.findViewById(R.id.btn_edit_memo);

            userId = AppController.preferences.getPreference_Int("pref_userid", -1);

        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            txtMemoText.setText(AppController.preferences.getPreference("pref_memotext", "Invalid memo"));

            Picasso.with(getActivity())
                    .load(AppController.preferences.getPreference("pref_memoimageurl", null))
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.error_placeholder)
                    .into(imgMemoImage);

            btnEditMemo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeScreenMethods.onEditMemoClick(getActivity(), getActivity().getSupportFragmentManager(), commonTitle, commonActionButton);
                }
            });

        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
    }

}
