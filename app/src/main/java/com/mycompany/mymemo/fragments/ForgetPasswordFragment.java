package com.mycompany.mymemo.fragments;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mycompany.mymemo.R;
import com.mycompany.mymemo.clickevents.LoginScreenMethods;

public class ForgetPasswordFragment extends Fragment {

    private Button btnForgetPasswordReset;
    private TextView commonTitle, commonActionButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forget_password, container, false);

        btnForgetPasswordReset = (Button) view.findViewById(R.id.btn_forgot_password_reset);

        commonTitle = (TextView) getActivity().findViewById(R.id.action_bar_title);
        commonActionButton = (TextView) getActivity().findViewById(R.id.action_bar_button);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnForgetPasswordReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginScreenMethods.onForgetPasswordResetClick(getActivity(), getActivity().getSupportFragmentManager(), commonTitle, commonActionButton);
            }
        });
    }
}
