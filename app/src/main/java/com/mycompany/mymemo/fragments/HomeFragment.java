package com.mycompany.mymemo.fragments;

import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mycompany.mymemo.R;
import com.mycompany.mymemo.app.AppController;
import com.mycompany.mymemo.components.CustomProgressBar;
import com.mycompany.mymemo.components.CustomToastMaker;
import com.mycompany.mymemo.components.RecyclerHomeListAdapter;
import com.mycompany.mymemo.constants.ClassNames;
import com.mycompany.mymemo.constants.URLConstants;
import com.mycompany.mymemo.json.JsonErrorsCheck;
import com.mycompany.mymemo.model.MemoDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private RecyclerView recyclerViewHome;
    private RecyclerHomeListAdapter recyclerAdapter;
    private RecyclerView.LayoutManager recyclerLayoutManager;

    private View view = null;
    private TextView txtEmptyList, commonTitle, commonActionButton;
    ;

    private List<MemoDetails> memoDetailsList;

    private String SUCCESS_RESPONSE = "Success";
    private int userId = AppController.preferences.getPreference_Int("pref_userid", -1);
    private String responseStatus, responseMessage;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        try {
            view = inflater.inflate(R.layout.fragment_home, container, false);
            userId = AppController.preferences.getPreference_Int("pref_userid", -1);

            txtEmptyList = (TextView) view.findViewById(R.id.txt_empty_list);
            recyclerViewHome = (RecyclerView) view.findViewById(R.id.recycler_list_view_home);
            commonTitle = (TextView) getActivity().findViewById(R.id.action_bar_title);
            commonActionButton = (TextView) getActivity().findViewById(R.id.action_bar_button);

        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.v("Home fragment...:- ", " Home Fragment resumed....!");

        try {

            JSONArray memoListArray;

            CustomProgressBar.showProgressDialog(getActivity(), "Loading memo list, please wait...", false);
            txtEmptyList.setVisibility(View.VISIBLE);

            recyclerLayoutManager = new LinearLayoutManager(getActivity());
            recyclerViewHome.setLayoutManager(recyclerLayoutManager);

            String memoListString = AppController.preferences.getPreference("pref_memoListString", "NULL");
            AppController.preferences.savePreference("pref_memoListString", "NULL");

            if (!(memoListString.equals("NULL") || memoListString.equals("-"))) {
                memoDetailsList = new ArrayList<>();
                memoListArray = new JSONArray(memoListString);
                getMemoListFromJsonArray(memoListArray);

                txtEmptyList.setVisibility(View.GONE);
                recyclerAdapter = new RecyclerHomeListAdapter(memoDetailsList, getActivity());
                recyclerViewHome.setAdapter(recyclerAdapter);
                recyclerAdapter.SetOnItemClickListener(new RecyclerHomeListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        onMemoItemClick(position);
                    }
                });
                CustomProgressBar.dismissProgressDialog();
            } else {
                // Get fresh list from api
                String uri = String.format(URLConstants.SHOW_MEMO_LIST + "?rquest=%1$s&user_Id=%2$s", "showmemolist", String.valueOf(userId));
                StringRequest stringRequest = new StringRequest(Request.Method.GET,
                        uri,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    Log.v("Response List...:", response);
                                    JSONObject responseObject = new JSONObject(response);
                                    responseStatus = responseObject.getString("status");
                                    responseMessage = responseObject.getString("msg");

                                    if (responseStatus.equals(SUCCESS_RESPONSE) && !responseMessage.equals("No Memo is there to show..")) {
                                        Log.v("ShowMemoObject...", responseObject.toString());
                                        JSONArray memoList = responseObject.getJSONArray("Memo List");

                                        if (memoList.length() <= 0) {
                                            Log.v("List status...", " Empty........." + responseMessage);
                                            CustomToastMaker.makeToast(getActivity(), responseMessage);
                                            txtEmptyList.setVisibility(View.VISIBLE);
                                        } else {

                                            Log.v("List status...", " Memo available........." + responseMessage);

                                            memoDetailsList = new ArrayList<>();

                                            getMemoListFromJsonArray(memoList);

                                            txtEmptyList.setVisibility(View.GONE);
                                            recyclerAdapter = new RecyclerHomeListAdapter(memoDetailsList, getActivity());
                                            recyclerViewHome.setAdapter(recyclerAdapter);
                                            recyclerAdapter.SetOnItemClickListener(new RecyclerHomeListAdapter.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(View view, int position) {
                                                    onMemoItemClick(position);
                                                }
                                            });

//                                            CustomProgressBar.dismissProgressDialog();
                                        }
                                    } else {
                                        Log.v("Response status...", responseMessage + " No list........." + responseStatus);
                                        CustomToastMaker.makeToast(getActivity(), responseMessage);
//                                        CustomProgressBar.dismissProgressDialog();
                                    }
                                } catch (Exception e) {
                                    CustomToastMaker.makeToast(getActivity(), " exception in response...!");
                                    e.printStackTrace();
                                } finally {
                                    CustomProgressBar.dismissProgressDialog();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                JsonErrorsCheck.whichErrorInJsonRequest(error);
                            }
                        });

                AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");

            }
        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        } finally {
            // CustomProgressBar.dismissProgressDialog();
        }
    }

    public void getMemoListFromJsonArray(JSONArray jsonArrayMemo) {
        try {
            for (int i = 0; i < jsonArrayMemo.length(); i++) {
                JSONObject currentMemo = jsonArrayMemo.getJSONObject(i);

                MemoDetails tempMemo = new MemoDetails();
                tempMemo.setMemoText(currentMemo.getString("Text"));
                tempMemo.setMemoImageUrl(currentMemo.getString("image_Url"));
                tempMemo.setContentId(currentMemo.getLong("Content_id"));
                tempMemo.setFavFlag(Byte.parseByte(currentMemo.getString("Fav_flag")));

                memoDetailsList.add(tempMemo);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onMemoItemClick(int position) {
        try {
            MemoDetails selectedMemo = memoDetailsList.get(position);
            if (selectedMemo != null) {
                AppController.preferences.savePreference("pref_memotext", selectedMemo.getMemoText());
                AppController.preferences.savePreference_Int("pref_favflag", selectedMemo.getFavFlag());
                AppController.preferences.savePreference("pref_memoimageurl", selectedMemo.getMemoImageUrl());
                AppController.preferences.savePreference("pref_contentid", selectedMemo.getContentId().toString());

                commonActionButton.setText("EDIT");
                commonActionButton.setTypeface(null);
                commonTitle.setText("SHOW MEMO");

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.fragment_place, new ShowMemoFragment())
                        .commit();
            } else
                CustomToastMaker.makeToast(getActivity(), "Memo element found null...!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
