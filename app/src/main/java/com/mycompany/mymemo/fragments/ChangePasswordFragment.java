package com.mycompany.mymemo.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mycompany.mymemo.R;
import com.mycompany.mymemo.clickevents.HomeScreenMethods;

public class ChangePasswordFragment extends Fragment {

    private Button btnSaveChangedPassword;
    private TextView txtCommonTitle, txtCommonActionButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);

        btnSaveChangedPassword = (Button) view.findViewById(R.id.btn_save_changed_password);

        txtCommonTitle = (TextView) getActivity().findViewById(R.id.action_bar_title);
        txtCommonActionButton = (TextView) getActivity().findViewById(R.id.action_bar_button);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnSaveChangedPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeScreenMethods.onSaveChangedPasswordClick(getActivity(), getActivity().getSupportFragmentManager(), txtCommonTitle, txtCommonActionButton);
            }
        });
    }
}
