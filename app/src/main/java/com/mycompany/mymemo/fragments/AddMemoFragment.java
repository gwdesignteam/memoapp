package com.mycompany.mymemo.fragments;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mycompany.mymemo.R;
import com.mycompany.mymemo.app.AppController;
import com.mycompany.mymemo.clickevents.HomeScreenMethods;
import com.mycompany.mymemo.components.CustomToastMaker;
import com.mycompany.mymemo.constants.ClassNames;

import java.io.ByteArrayOutputStream;

public class AddMemoFragment extends Fragment {

    private static int RESULT_LOAD_IMG = 1;

    private Button btnSaveAddedMemo;
    private TextView commonTitle, commonActionButton;
    private ImageView imgAddMemo;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_memo, container, false);

        btnSaveAddedMemo = (Button) view.findViewById(R.id.btn_save_added_memo);
        imgAddMemo = (ImageView) view.findViewById(R.id.img_add_memo);

        commonTitle = (TextView) getActivity().findViewById(R.id.action_bar_title);
        commonActionButton = (TextView) getActivity().findViewById(R.id.action_bar_button);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        imgAddMemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppController.preferences.savePreference("pref_setimage", "add");
                Log.v("Img set status...", "Image adding...");
                // Create intent to Open Image applications like Gallery, Google Photos
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                // Start the Intent
                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
            }
        });

        btnSaveAddedMemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeScreenMethods.onSaveAddedMemoClick(getActivity(), getActivity().getSupportFragmentManager(), commonTitle, commonActionButton);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == Activity.RESULT_OK && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getContext().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imgDecodableString = cursor.getString(columnIndex);
                System.out.println(imgDecodableString);
                cursor.close();

//-------- Converting the image to Base64 byteArray....----------------------------------------

                Bitmap bitmap = BitmapFactory.decodeFile(imgDecodableString);
                ByteArrayOutputStream bitmapInBytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bitmapInBytes);
                bitmapInBytes.toByteArray();

//                byte[] base64Bytes = Base64.encode(bitmapInBytes.toByteArray(), Base64.DEFAULT);
//                String base64String = Base64.encodeToString(bitmapInBytes.toByteArray(), Base64.DEFAULT);
//---------------------------------------------------------------------------------------------

                    imgAddMemo.setImageBitmap(bitmap);
                // Set the Image in ImageView after decoding the String
//                imgView.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
            } else {
                CustomToastMaker.makeToast(getActivity(), "You haven't picked Image");
            }
        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
