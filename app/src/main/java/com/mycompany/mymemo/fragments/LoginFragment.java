package com.mycompany.mymemo.fragments;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mycompany.mymemo.R;
import com.mycompany.mymemo.clickevents.LoginScreenMethods;

public class LoginFragment extends Fragment {

    private Button btnLogin, btnForgetPassword;
    private TextView commonTitle, commonActionButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        btnLogin = (Button) view.findViewById(R.id.btn_login);
        btnForgetPassword = (Button) view.findViewById(R.id.btn_forget_password);

        commonTitle = (TextView) getActivity().findViewById(R.id.action_bar_title);
        commonActionButton = (TextView) getActivity().findViewById(R.id.action_bar_button);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginScreenMethods.onLoginClick(getActivity());
            }
        });

        btnForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                commonTitle.setText("FORGOT PASSWORD");
                commonActionButton.setText("");
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_place, new ForgetPasswordFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });
    }
}
