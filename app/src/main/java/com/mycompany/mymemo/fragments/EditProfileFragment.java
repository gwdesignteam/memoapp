package com.mycompany.mymemo.fragments;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mycompany.mymemo.R;
import com.mycompany.mymemo.app.AppController;
import com.mycompany.mymemo.clickevents.HomeScreenMethods;
import com.mycompany.mymemo.constants.ClassNames;

public class EditProfileFragment extends Fragment {

    private EditText edtName, edtEmail, edtUserType;
    private Button btnEditSaveProfile;
    private TextView txtCommonTitle, txtCommonActionButton, txtDrawerName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        edtName = (EditText) view.findViewById(R.id.edit_name);
        edtEmail = (EditText) view.findViewById(R.id.edit_email);
        edtUserType = (EditText) view.findViewById(R.id.edit_user_type);
        btnEditSaveProfile = (Button) view.findViewById(R.id.btn_edit_save_profile);

        txtCommonTitle = (TextView) getActivity().findViewById(R.id.action_bar_title);
        txtCommonActionButton = (TextView) getActivity().findViewById(R.id.action_bar_button);
        txtDrawerName = (TextView) getActivity().findViewById(R.id.txt_header_name);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            edtName.setText(AppController.preferences.getPreference("pref_name", "No name"));
            edtEmail.setText(AppController.preferences.getPreference("pref_email", "No email"));
            edtUserType.setText(AppController.preferences.getPreference("pref_usertype", "No user type"));

            btnEditSaveProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    HomeScreenMethods.onEditOrSaveProfileClick(getActivity(), getActivity().getSupportFragmentManager(), txtCommonTitle, txtCommonActionButton, txtDrawerName);
                }
            });
        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
