package com.mycompany.mymemo.json;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

/**
 * Created by sushil on 31/12/15.
 */
public class JsonErrorsCheck {
    public static void whichErrorInJsonRequest(VolleyError error) {
        try {
            if (error instanceof NoConnectionError) {
                Log.d("NoConnectionError>>>>>>", "NoConnectionError.......");
//                            ErrorMessage.logErrorMessage(getString(R.string.connection_error_msg), context);
            } else if (error instanceof AuthFailureError) {
                Log.d("AuthFailureError>>>>>>", "AuthFailureError.......");
//                            ErrorMessage.logErrorMessage(getString(R.string.authFailure_error_msg), context);
            } else if (error instanceof ServerError) {
                Log.d("ServerError>>>>>>>>>", "ServerError.......");
//                            ErrorMessage.logErrorMessage(getString(R.string.server_connection_error_msg), context);
            } else if (error instanceof NetworkError) {
                Log.d("NetworkError>>>>>>>>>", "NetworkError.......");
//                            ErrorMessage.logErrorMessage(getString(R.string.network_error_msg), context);
            } else if (error instanceof ParseError) {
                Log.d("ParseError>>>>>>>>>", "ParseError.......");
//                            ErrorMessage.logErrorMessage(getString(R.string.parse_error_msg), context);
            } else if (error instanceof TimeoutError) {
                Log.d("TimeoutError>>>>>>>>>", "TimeoutError.......");
//                            ErrorMessage.logErrorMessage(getString(R.string.timeout_error_msg), context);
            }
        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class - JsonErrorsCheck " + " :: " + e.getMessage());
            e.printStackTrace();
        }

    }
}
