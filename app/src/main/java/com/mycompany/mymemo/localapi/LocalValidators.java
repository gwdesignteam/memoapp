package com.mycompany.mymemo.localapi;

import java.util.regex.Pattern;

/**
 * Created by sushil on 22/12/15.
 */
public class LocalValidators {
    private static final Pattern EMAIL_PATTERN = Pattern.compile
//            ("[a-zA-Z0-9+._%-+]{1,256}" + "@" + "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
//                    "(" + "." + "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" + ")+");

            ("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    private static final Pattern MOBILE_NUMBER_PATTERN = Pattern.compile("^[789]\\d{9}$");

    private static final Pattern NAME_PATTERN = Pattern.compile("/^[A-Za-z ]+$/");

    public static boolean isValidEmail(String email){
        return EMAIL_PATTERN.matcher(email).matches();
    }

    public static boolean isMobileNoValid(String mobNo){
        return MOBILE_NUMBER_PATTERN.matcher(mobNo).matches();
    }

    public static boolean isNameValid(String name){
        return NAME_PATTERN.matcher(name).matches();
    }

}
