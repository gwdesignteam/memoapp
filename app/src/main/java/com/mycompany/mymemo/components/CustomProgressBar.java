package com.mycompany.mymemo.components;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

/**
 * Created by sushil on 22/12/15.
 */
public class CustomProgressBar {

    private static ProgressDialog progressDialog;

    public static void showProgressDialog(Activity context, String message, boolean isCancelable) {
        Log.e("Progress bar ....", "Progress bar showing..........");
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(isCancelable);
//        progressDialog.setTitle("Loading");
        progressDialog.show();
    }

    public static void dismissProgressDialog() {
        Log.e("Progress bar ....", "Progress bar dismissed..........");
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }
}
