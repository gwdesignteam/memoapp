package com.mycompany.mymemo.components;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mycompany.mymemo.R;
import com.mycompany.mymemo.app.AppController;
import com.mycompany.mymemo.constants.URLConstants;
import com.mycompany.mymemo.json.JsonErrorsCheck;
import com.mycompany.mymemo.model.MemoDetails;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by sushil on 29/12/15.
 */
public class RecyclerHomeListAdapter extends RecyclerView.Adapter<RecyclerHomeListAdapter.CustomViewHolder> {

    private List<MemoDetails> memoItemList;
    private Activity context;
    private OnItemClickListener mItemClickListener;

    private String SUCCESS_RESPONSE = "Success";
    private String responseStatus, responseMessage;

    private Typeface tf1Fav, tf1Del;

    private int colorFav;
    private int colorNonFav;

    private int userId = AppController.preferences.getPreference_Int("pref_userid", -1);

    public RecyclerHomeListAdapter(List<MemoDetails> memoItemList, Activity context) {
        this.memoItemList = memoItemList;
        this.context = context;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView memoImage;
        public TextView memoText, memoFav, memoRemove;

        public CustomViewHolder(View itemView) {
            super(itemView);
            this.memoImage = (ImageView) itemView.findViewById(R.id.img_recycler_list);
            this.memoText = (TextView) itemView.findViewById(R.id.txt_recycler_list);
            this.memoFav = (TextView) itemView.findViewById(R.id.txt_fav_button);
            this.memoRemove = (TextView) itemView.findViewById(R.id.txt_del_button);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    //-----------------------------------------------------------------------------------------------
    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_list_layout, parent, false);
        CustomViewHolder customViewHolder = new CustomViewHolder(view);
        return customViewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {

        userId = AppController.preferences.getPreference_Int("pref_userid", -1);

        colorFav = context.getResources().getColor(R.color.Green);
        colorNonFav = context.getResources().getColor(R.color.Gray);

        tf1Fav = Typeface.createFromAsset(context.getAssets(), "heart_fill_icon.ttf");
        tf1Del = Typeface.createFromAsset(context.getAssets(), "delete_icon.ttf");
        final MemoDetails memoTemp = memoItemList.get(position);

        Picasso.with(context)
                .load(memoTemp.getMemoImageUrl())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error_placeholder)
                .into(holder.memoImage);

        holder.memoText.setText(memoTemp.getMemoText());

        if (memoTemp.getFavFlag() == 0)
            holder.memoFav.setTextColor(colorNonFav);
        else if (memoTemp.getFavFlag() == 1)
            holder.memoFav.setTextColor(colorFav);
        holder.memoFav.setText("F");
        holder.memoFav.setTypeface(tf1Fav);

        holder.memoFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    CustomProgressBar.showProgressDialog(context, "Please wait...", false);
                    int currentColor = holder.memoFav.getCurrentTextColor();
                    final byte fav = (byte) (currentColor == context.getResources().getColor(R.color.Green) ? 1 : 0);

                    String uri = String.format(URLConstants.SET_FAV_MEMO + "?rquest=%1$s&user_Id=%2$s&content_Id=%3$s&Fav_flag=%4$s",
                            "favmemo",
                            userId,
                            memoTemp.getContentId(),
                            fav);

                    StringRequest stringRequest = new StringRequest(Request.Method.GET,
                            uri,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject responseObject = new JSONObject(response);

                                        Log.v("Favourite Response... ", responseObject.toString());

                                        responseStatus = responseObject.getString("status");
                                        responseMessage = responseObject.getString("msg");

                                        if (responseStatus.equals(SUCCESS_RESPONSE)) {// && responseMessage.trim().equals(" Memo status has been changed ..".trim())) {
                                            if (fav == 0) { //switch flag so Red for local 0
                                                holder.memoFav.setTextColor(colorFav);

                                            } else if (fav == 1) { //switch flag so Gray for local 1
                                                holder.memoFav.setTextColor(colorNonFav);
                                            }
                                        }

                                        Log.v("Favourite status...", responseMessage);
                                        CustomToastMaker.makeToast(context, "Favourite status..." + responseMessage);

                                    } catch (JSONException e) {
                                        Log.e("JSONException...", "You got json exception on set favourite response");
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } finally {
                                        CustomProgressBar.dismissProgressDialog();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    JsonErrorsCheck.whichErrorInJsonRequest(error);
                                }
                            });

                    AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");

//                    CustomToastMaker.makeToast(context, "You clicked on favourite button...!");
                    Log.v("Fav clicked... ", " You clicked on favourite button...!");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        holder.memoRemove.setText("D");
        holder.memoRemove.setTextColor(context.getResources().getColor(R.color.Red));
        holder.memoRemove.setTypeface(tf1Del);
        holder.memoRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String uri = String.format(URLConstants.REMOVE_MEMO + "?rquest=%1$s&user_Id=%2$s&content_Id=%3$s",
                            "removememo",
                            userId,
                            memoTemp.getContentId());

                    StringRequest stringRequest = new StringRequest(Request.Method.GET,
                            uri,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        JSONObject responseObject = new JSONObject(response);

                                        Log.v("Remove Response... ", responseObject.toString());

                                        responseStatus = responseObject.getString("status");
                                        responseMessage = responseObject.getString("msg");

                                        if (responseStatus.equals(SUCCESS_RESPONSE)) {

                                            memoItemList.remove(position);
                                            notifyDataSetChanged();

                                            Log.e("Remove status...", "The memo has been removed...!");

                                        }

                                        Log.v("Remove status...", responseMessage);
                                        CustomToastMaker.makeToast(context, "Remove status..." + responseMessage);
                                    } catch (JSONException e) {
                                        Log.e("JSONException...", "You got json exception on remove response");
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    JsonErrorsCheck.whichErrorInJsonRequest(error);
                                }
                            });
                    AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != memoItemList ? memoItemList.size() : 0);
    }
}
