package com.mycompany.mymemo.components;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.mycompany.mymemo.constants.ClassNames;

/**
 * Created by sushil on 22/12/15.
 */
public class CustomToastMaker {
    public static void makeToast(Activity context, String toastMessage) {
        try {
            Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            Log.e("Exception : ", "Exception in class " + "CustomToastMaker" + " :: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
