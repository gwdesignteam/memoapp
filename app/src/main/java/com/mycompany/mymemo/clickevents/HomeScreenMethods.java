package com.mycompany.mymemo.clickevents;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mycompany.mymemo.R;
import com.mycompany.mymemo.app.AppController;
import com.mycompany.mymemo.components.CustomToastMaker;
import com.mycompany.mymemo.constants.ClassNames;
import com.mycompany.mymemo.constants.URLConstants;
import com.mycompany.mymemo.fragments.EditMemoFragment;
import com.mycompany.mymemo.json.JsonErrorsCheck;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sushil on 8/1/16.
 */
public class HomeScreenMethods {
    private static int userId = AppController.preferences.getPreference_Int("pref_userid", -1);
    private static String responseStatus, responseMessage, userName;
    private static final String SUCCESS_RESPONSE = "Success";

    private static Typeface tf1;

    public static void onRemoveEditedMemoClick(final Activity activity, final FragmentManager fragmentManager, final TextView commonTitle, final TextView commonActionButton) {
        try {

            tf1 = Typeface.createFromAsset(activity.getAssets(), "toolbar_icons.ttf");

            String uri = String.format(URLConstants.REMOVE_MEMO + "?rquest=%1$s&user_Id=%2$s&content_Id=%3$s",
                    "removememo",
                    userId,
                    AppController.preferences.getPreference("pref_contentid", "invalid"));

            StringRequest stringRequest = new StringRequest(Request.Method.GET,
                    uri,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject responseObject = new JSONObject(response);

                                Log.v("Remove Response... ", responseObject.toString());

                                responseStatus = responseObject.getString("status");
                                responseMessage = responseObject.getString("msg");

                                if (responseStatus.equals(SUCCESS_RESPONSE)) {

                                    fragmentManager.popBackStackImmediate();
                                    fragmentManager.popBackStackImmediate();

                                    commonActionButton.setText("A");
                                    commonActionButton.setTypeface(tf1);
                                    commonTitle.setText("HOME");
                                }
                                CustomToastMaker.makeToast(activity, "Remove status..." + responseMessage);
                            } catch (JSONException e) {
                                Log.e("JSONException...", "You got json exception on remove response");
                                e.printStackTrace();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            JsonErrorsCheck.whichErrorInJsonRequest(error);
                        }
                    });
            AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");

        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void onSaveEditedMemoClick(final Activity activity, final FragmentManager fragmentManager, final TextView commonTitle, final TextView commonActionButton) {
        try {

            tf1 = Typeface.createFromAsset(activity.getAssets(), "toolbar_icons.ttf");

            EditText edtEditMemo = (EditText) activity.findViewById(R.id.edt_edit_memo_text);
            ImageView imgEditMemo = (ImageView) activity.findViewById(R.id.img_edit_memo);

            final String memoText = edtEditMemo.getText().toString();

            BitmapDrawable drawableMemo = (BitmapDrawable) imgEditMemo.getDrawable();
            Bitmap bitmapMemo = drawableMemo.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmapMemo.compress(Bitmap.CompressFormat.JPEG, 90, stream);
            byte[] bytesMemo = stream.toByteArray();
            Log.v("Image byte array... ", bytesMemo.toString());
            final String imageInBase64 = Base64.encodeToString(bytesMemo, Base64.DEFAULT);

            StringRequest stringRequest = new StringRequest(Request.Method.POST,
                    URLConstants.EDIT_MEMO,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject responseObject = new JSONObject(response);
                                responseMessage = responseObject.getString("msg");
                                responseStatus = responseObject.getString("status");

                                if (responseStatus.equals(SUCCESS_RESPONSE)) {
                                    CustomToastMaker.makeToast(activity, "Memo Edited successfully..!");
                                    AppController.preferences.savePreference("pref_memotext", memoText);
//                                    AppController.preferences.savePreference("pref_memoimageurl", selectedMemo.getMemoImageUrl());

                                    fragmentManager.popBackStackImmediate();
                                    fragmentManager.popBackStackImmediate();

                                    commonActionButton.setText("A");
                                    commonActionButton.setTypeface(tf1);
                                    commonTitle.setText("HOME");

                                } else {
                                    CustomToastMaker.makeToast(activity, responseMessage);
                                }
                            } catch (Exception e) {
                                Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            JsonErrorsCheck.whichErrorInJsonRequest(error);
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> postParam = new HashMap<>();
                    postParam.put("rquest", "editmemo");
                    postParam.put("user_Id", String.valueOf(userId));
                    postParam.put("content_Id", AppController.preferences.getPreference("pref_contentid", "invalid"));
                    postParam.put("image_Url", imageInBase64);
                    postParam.put("text", memoText);
                    return postParam;
                }
            };

            AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");

        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void onSaveAddedMemoClick(final Activity activity, final FragmentManager fragmentManager, final TextView commonTitle, final TextView commonActionButton){
        try {
            tf1 = Typeface.createFromAsset(activity.getAssets(), "toolbar_icons.ttf");

            EditText editAddMemo = (EditText) activity.findViewById(R.id.edt_add_memo_text);
            ImageView imgAddMemo = (ImageView) activity.findViewById(R.id.img_add_memo);

            final String memoText = editAddMemo.getText().toString();

            BitmapDrawable drawableMemo = (BitmapDrawable) imgAddMemo.getDrawable();
            Bitmap bitmapMemo = drawableMemo.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmapMemo.compress(Bitmap.CompressFormat.JPEG, 90, stream);
            byte[] bytesMemo = stream.toByteArray();
            Log.v("Image byte array... ", bytesMemo.toString());
            final String imageInBase64 = Base64.encodeToString(bytesMemo, Base64.DEFAULT);

            StringRequest stringRequest = new StringRequest(Request.Method.POST,
                    URLConstants.ADD_MEMO,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject responseObject = new JSONObject(response);
                                responseMessage = responseObject.getString("msg");
                                responseStatus = responseObject.getString("status");

                                if (responseStatus.equals(SUCCESS_RESPONSE)) {
                                    CustomToastMaker.makeToast(activity, "Memo Added successfully..!");
                                    commonActionButton.setText("A");
                                    commonActionButton.setTypeface(tf1);
                                    commonTitle.setText("HOME");
                                    fragmentManager.popBackStackImmediate();
                                } else {
                                    CustomToastMaker.makeToast(activity, responseMessage);
                                }
                            } catch (Exception e) {
                                Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            JsonErrorsCheck.whichErrorInJsonRequest(error);
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> postParam = new HashMap<>();
                    postParam.put("rquest", "addmemo");
                    postParam.put("user_Id", String.valueOf(userId));
                    postParam.put("text", memoText);
                    postParam.put("image_Url", imageInBase64);
                    return postParam;
                }
            };

            AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");

        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void onEditMemoClick(final Activity activity, final FragmentManager fragmentManager, final TextView commonTitle, final TextView commonActionButton) {
        try {
            commonActionButton.setText("SAVE");
            commonActionButton.setTypeface(null);
            commonTitle.setText("EDIT MEMO");

            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_place, new EditMemoFragment())
                    .addToBackStack(null)
                    .commit();

        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void onEditOrSaveProfileClick(final Activity activity, final FragmentManager fragmentManager, final TextView commonTitle, final TextView commonActionButton, final TextView drawerName){
        try {
            tf1 = Typeface.createFromAsset(activity.getAssets(), "toolbar_icons.ttf");

            Button btnSaveEdit = (Button) activity.findViewById(R.id.btn_edit_save_profile);
            String btnText = btnSaveEdit.getText().toString();

            EditText nameEditText = (EditText) activity.findViewById(R.id.edit_name);

            if (btnText.toLowerCase().equals("edit")) {
                CustomToastMaker.makeToast(activity, "You can edit the name only...!");
                nameEditText.setEnabled(true);
                btnSaveEdit.setText("SAVE");
                commonActionButton.setText("SAVE");
                commonActionButton.setTypeface(null);
            }

            if (btnText.toLowerCase().equals("save")) {
                String errorMessage = "Please enter valid";
                userName = nameEditText.getText().toString();
                if (userName.trim().equals("")) {
                    errorMessage += " User name";
                }
                if (!errorMessage.equals("Please enter valid")) {
                    CustomToastMaker.makeToast(activity, errorMessage);
                    return;
                }

                String uri = String.format(URLConstants.EDIT_PROFILE + "?rquest=%1$s&user_Id=%2$s&newname=%3$s",
                        "myprofileupdate_details",
                        String.valueOf(userId),
                        userName);

                StringRequest stringRequest = new StringRequest(Request.Method.GET,
                        uri,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject responseObject = new JSONObject(response);
                                    responseStatus = responseObject.getString("status");
                                    responseMessage = responseObject.getString("msg");

                                    if (responseStatus.equals(SUCCESS_RESPONSE)) {
                                        CustomToastMaker.makeToast(activity, "Profile updated successfully..!");
                                        AppController.preferences.savePreference("pref_name", userName);

                                        drawerName.setText(userName);

                                        commonActionButton.setText("A");
                                        commonActionButton.setTypeface(tf1);
                                        commonTitle.setText("HOME");

                                        fragmentManager.popBackStackImmediate();

                                    } else {
                                        CustomToastMaker.makeToast(activity, responseMessage);
                                    }
                                } catch (Exception e) {
                                    Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                JsonErrorsCheck.whichErrorInJsonRequest(error);
                            }
                        });

                AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");
            }
        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void onSaveChangedPasswordClick(final Activity activity, final FragmentManager fragmentManager, final TextView commonTitle, final TextView commonActionButton){
        try {
            tf1 = Typeface.createFromAsset(activity.getAssets(), "toolbar_icons.ttf");

            String errorMessage = "Please enter valid";
            EditText editOldPassword = (EditText) activity.findViewById(R.id.edit_old_password);
            final String oldPassword = editOldPassword.getText().toString();

            EditText editNewPassword = (EditText) activity.findViewById(R.id.edit_new_password);
            final String newPassword = editNewPassword.getText().toString();

            EditText editConfirmNewPassword = (EditText) activity.findViewById(R.id.edit_confirm_new_password);
            String confirmNewPassword = editConfirmNewPassword.getText().toString();

            if (oldPassword.equals("") || oldPassword.length() < 6 || oldPassword.length() > 25) {
                errorMessage += " Old Password";
            }
            if (newPassword.equals("") || newPassword.length() < 6 || newPassword.length() > 25) {
                errorMessage += " New Password (6-25 chars)";
            }
            if (confirmNewPassword.equals("") || confirmNewPassword.length() < 6 || confirmNewPassword.length() > 25) {
                errorMessage += " Confirm New Password (6-25 chars)";
            }

            if (!errorMessage.equals("Please enter valid")) {
                CustomToastMaker.makeToast(activity, errorMessage);
                return;
            }

            if (!newPassword.equals(confirmNewPassword)) {
                CustomToastMaker.makeToast(activity, "New password and confirm password should be same, please check");
                return;
            }

            StringRequest stringRequest = new StringRequest(Request.Method.POST,
                    URLConstants.CHANGE_PASSWORD,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject responseObject = new JSONObject(response);
                                responseStatus = responseObject.getString("status");
                                responseMessage = responseObject.getString("msg");

                                if (responseStatus.equals(SUCCESS_RESPONSE)) {
                                    CustomToastMaker.makeToast(activity, "Password changed successfully..!");
                                    commonActionButton.setText("A");
                                    commonActionButton.setTypeface(tf1);
                                    commonTitle.setText("HOME");

                                    fragmentManager.popBackStackImmediate();

                                } else {
                                    CustomToastMaker.makeToast(activity, responseMessage);
                                }
                            } catch (Exception e) {
                                Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            JsonErrorsCheck.whichErrorInJsonRequest(error);
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> postParam = new HashMap<>();
                    postParam.put("rquest", "changepass");
                    postParam.put("user_Id", String.valueOf(userId));
                    postParam.put("password", oldPassword);
                    postParam.put("newpassword", newPassword);
                    return postParam;
                }
            };

            AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");

        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
    }
}