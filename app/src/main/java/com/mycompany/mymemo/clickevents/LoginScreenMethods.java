package com.mycompany.mymemo.clickevents;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mycompany.mymemo.R;
import com.mycompany.mymemo.activities.HomeActivity;
import com.mycompany.mymemo.app.AppController;
import com.mycompany.mymemo.components.CustomProgressBar;
import com.mycompany.mymemo.components.CustomToastMaker;
import com.mycompany.mymemo.constants.ClassNames;
import com.mycompany.mymemo.constants.URLConstants;
import com.mycompany.mymemo.fragments.ForgetPasswordFragment;
import com.mycompany.mymemo.fragments.LoginFragment;
import com.mycompany.mymemo.json.JsonErrorsCheck;
import com.mycompany.mymemo.localapi.LocalValidators;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sushil on 8/1/16.
 */
public class LoginScreenMethods {

    private static String emailId = "", password = "", responseStatus, responseMessage, userName, userEmail, userType;
    private static final String SUCCESS_RESPONSE = "Success";
    private static int userId, a;

    public static void onLoginClick(final Activity activity) {
        try {

            CustomProgressBar.showProgressDialog(activity, "Please wait...", false);

            String errorMessage = "Please enter valid";

            EditText emailEditText = (EditText) activity.findViewById(R.id.edit_email_login);
            emailId = emailEditText.getText().toString();

            if (emailId.equals("") || !LocalValidators.isValidEmail(emailId)) {
                errorMessage += " Email-id";
            }

            EditText passwordEditText = (EditText) activity.findViewById(R.id.edit_password);
            password = passwordEditText.getText().toString();

            if (password.equals("") || password.length() < 6 || password.length() > 25) {
                errorMessage += " Password";
            }

            if (!errorMessage.equals("Please enter valid")) {
                CustomToastMaker.makeToast(activity, errorMessage);
                CustomProgressBar.dismissProgressDialog();
                return;
            }

// ------------------- string request -------------------
            StringRequest stringRequest = new StringRequest(Request.Method.POST,
                    URLConstants.LOGIN,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                Log.v("Response.....:", response);

                                JSONObject responseObject = new JSONObject(response);
                                responseStatus = responseObject.getString("status");
                                responseMessage = responseObject.getString("msg");

                                if (responseStatus.equals(SUCCESS_RESPONSE)) {
                                    JSONObject userDetails = responseObject.getJSONObject("Login");
                                    userId = userDetails.getInt("User_id");
                                    userName = userDetails.getString("Name");
                                    userEmail = userDetails.getString("Email");
                                    userType = userDetails.getString("User_type");

                                    AppController.preferences.savePreference("pref_email", userEmail);
                                    AppController.preferences.savePreference("pref_name", userName);
                                    AppController.preferences.savePreference("pref_usertype", userType);
                                    AppController.preferences.savePreference_Int("pref_userid", userId);

                                    JSONArray memoList = responseObject.getJSONArray("Memo List");
                                    Log.v("Id.....", "" + userId);
                                    Log.v("Name...", userName);
                                    Log.v("Email..", userEmail);
                                    Log.v("Type....", userType);
                                    Log.v("List...", memoList.toString());
                                    if (memoList.length() <= 0) {
                                        AppController.preferences.savePreference("pref_memoListString", "-");
                                    } else {
                                        AppController.preferences.savePreference("pref_memoListString", memoList.toString());
                                    }
//                                    CustomProgressBar.dismissProgressDialog();

                                    Intent intent = new Intent(activity, HomeActivity.class);
                                    activity.startActivity(intent);

                                } else {
                                    CustomToastMaker.makeToast(activity, responseMessage);
//                                    CustomProgressBar.dismissProgressDialog();
                                }
                            } catch (Exception e) {
                                CustomToastMaker.makeToast(activity, " exception in response...!");
                                e.printStackTrace();
                            } finally {
                                CustomProgressBar.dismissProgressDialog();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            JsonErrorsCheck.whichErrorInJsonRequest(error);
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> postParam = new HashMap<>();
                    postParam.put("rquest", "login");
                    postParam.put("email", emailId);
                    postParam.put("password", password);
                    return postParam;
                }
            };
            Log.v("Response", "...... Before service call");
            AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");
            Log.v("Response", "...... After service call");
        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.LOGIN_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        } finally {
//            CustomProgressBar.dismissProgressDialog();
        }
    }

    public static void onForgetPasswordResetClick(final Activity activity, final FragmentManager fragmentManager, final TextView commonTitle, final TextView commonActionButton) {
        try {

            EditText editText_email = (EditText) activity.findViewById(R.id.edit_email_forget);
            emailId = editText_email.getText().toString();

            if (!LocalValidators.isValidEmail(emailId)) {
                CustomToastMaker.makeToast(activity, "Please enter valid email Id...!");
                return;
            }

            StringRequest stringRequest = new StringRequest(Request.Method.POST, URLConstants.FORGET_PASSWORD,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                responseStatus = jsonObject.getString("status");
                                responseMessage = jsonObject.getString("msg");

                                if (responseStatus.equals("Success")) {
                                    CustomToastMaker.makeToast(activity, "New password sent to your mail id...!");

                                    commonTitle.setText("LOGIN");
                                    commonActionButton.setText("LOGIN");

                                    fragmentManager.popBackStackImmediate();
                                    fragmentManager.beginTransaction()
                                            .replace(R.id.fragment_place, new LoginFragment())
                                            .commit();
                                } else {
                                    CustomToastMaker.makeToast(activity, responseMessage);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            JsonErrorsCheck.whichErrorInJsonRequest(error);
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> postParam = new HashMap<>();
                    postParam.put("rquest", "resetpass");
                    postParam.put("email", emailId);
                    return postParam;
                }
            };

            AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");

        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.LOGIN_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
    }

}
