package com.mycompany.mymemo.handlers;

import android.content.Context;

import com.mycompany.mymemo.app.AppController;

/**
 * Created by sushil on 22/12/15.
 */
public class ApplicationPreferences {
    private Context context;
    private String preferenceName = "memoproject";

    public ApplicationPreferences(Context context) {
        this.context = context;
    }

    public void savePreference(String key, String value) {

        context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).edit().putString(key, value).commit();
    }

    public void savePreference_Int(String key, int value) {

        context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).edit().putInt(key, value).commit();
    }


    public void savePreference_Boolean(String key, boolean value) {

        context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).edit().putBoolean(key, value).commit();
    }


    public String getPreference(String key, String defaultValue) {

        return context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).getString(key, defaultValue);

    }

    public int getPreference_Int(String key, int defaultValue) {

        return context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).getInt(key, defaultValue);

    }

    public boolean getPreference_Boolean(String key, boolean defaultValue) {

        return context.getSharedPreferences(preferenceName, Context.MODE_PRIVATE).getBoolean(key, defaultValue);

    }

}
