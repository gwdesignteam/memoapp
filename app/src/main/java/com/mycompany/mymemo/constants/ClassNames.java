package com.mycompany.mymemo.constants;

/**
 * Created by sushil on 23/12/15.
 */
public class ClassNames {
    public static String LOGIN_ACTIVITY = "LoginActivity";
    public static String HOME_ACTIVITY = "HomeActivity";
    public static String MAIN_ACTIVITY = "MainActivity";
    public static String REGISTER_ACTIVITY = "RegisterActivity";

}
