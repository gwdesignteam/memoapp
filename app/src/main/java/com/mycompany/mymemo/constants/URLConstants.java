package com.mycompany.mymemo.constants;

/**
 * Created by sushil on 22/12/15.
 */
public class URLConstants {
    public static String DOMAIN = "http://dev.geekyworks.com/memo/";
    public static String LOGIN = DOMAIN + "loginapi.php";
    public static String REGISTER = DOMAIN + "registerapi.php";
    public static String FORGET_PASSWORD = DOMAIN + "resetpassapi.php";
    public static String SHOW_MEMO_LIST = DOMAIN + "showmemolist_api.php";
    public static String ADD_MEMO = DOMAIN + "addmemoapi.php";
    public static String CHANGE_PASSWORD = DOMAIN + "changepass_api.php";
    public static String EDIT_MEMO = DOMAIN + "editmemo_api.php";
    public static String EDIT_PROFILE = DOMAIN + "myprofileupdatedetails_api.php";
    public static String SET_FAV_MEMO = DOMAIN + "favoritememo_api.php";
    public static String REMOVE_MEMO = DOMAIN + "removememo_api.php";
    public static String DELETE_MEMO = DOMAIN + "deletememo_api.php";
    public static String COMPLETED_MEMO_LIST = DOMAIN + "completedmemolist_api.php";

}