package com.mycompany.mymemo.model;

/**
 * Created by sushil on 30/12/15.
 */
public class MemoDetails {

    private Long contentId;
    private String memoText;
    private String memoImageUrl;
    private byte favFlag;

    public String getMemoText() {
        return memoText;
    }

    public void setMemoText(String memoText) {
        this.memoText = memoText;
    }

    public String getMemoImageUrl() {
        return memoImageUrl;
    }

    public void setMemoImageUrl(String memoImageUrl) {
        this.memoImageUrl = memoImageUrl;
    }

    public Long getContentId() {
        return contentId;
    }

    public void setContentId(Long contentId) {
        this.contentId = contentId;
    }

    public byte getFavFlag() {
        return favFlag;
    }

    public void setFavFlag(byte favFlag) {
        this.favFlag = favFlag;
    }
}
