package com.mycompany.mymemo.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.mycompany.mymemo.R;
import com.mycompany.mymemo.app.AppController;
import com.mycompany.mymemo.clickevents.HomeScreenMethods;
import com.mycompany.mymemo.components.CustomProgressBar;
import com.mycompany.mymemo.constants.ClassNames;
import com.mycompany.mymemo.fragments.AddMemoFragment;
import com.mycompany.mymemo.fragments.ChangePasswordFragment;
import com.mycompany.mymemo.fragments.EditProfileFragment;
import com.mycompany.mymemo.fragments.HomeFragment;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private Typeface tf1;
//    private LayoutInflater layoutInflater;
    private TextView commonTitle, commonActionButton, drawerIcon,
            drawerName, drawerEmail;

//    private EditText editOldPassword, editNewPassword, editConfirmNewPassword, editAddMemo,
//            edtEditMemo;
//    private ImageView imgAddMemo, imgEditMemo;
//    private String emailId = "", password = "", userName = "", userType = "",
//            imgDecodableString, oldPassword = "", newPassword = "", confirmNewPassword = "",
//            memoText, memoImageByteCode, imageInBase64,
//            responseStatus, responseMessage;
//    private String SUCCESS_RESPONSE = "Success";
//    private int userId = AppController.preferences.getPreference_Int("pref_userid", -1);

    public static DrawerLayout drawer;

    //--------------------------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Log.v("Home Created...:- ", " Home Activity created....!");

//        userId = AppController.preferences.getPreference_Int("pref_userid", -1);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, null, R.string.openDrawer, R.string.closeDrawer);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        commonTitle = (TextView) findViewById(R.id.action_bar_title);
        commonActionButton = (TextView) findViewById(R.id.action_bar_button);

        drawerName = (TextView) findViewById(R.id.txt_header_name);
        drawerName.setText(AppController.preferences.getPreference("pref_name", "No name"));
        drawerEmail = (TextView) findViewById(R.id.txt_header_email);
        drawerEmail.setText(AppController.preferences.getPreference("pref_email", "abc@gmail.com"));
//-------------------- inflate the custom action bar -----------------------
        try {
//            -------------------- action bar -----------------------

            tf1 = Typeface.createFromAsset(getAssets(), "toolbar_icons.ttf");

            toolbar = (Toolbar) findViewById(R.id.mycustom_toolbar);
            setSupportActionBar(toolbar);

            commonTitle.setText("HOME");

            commonActionButton.setText("A");
            commonActionButton.setTypeface(tf1);
            commonActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (drawer.isDrawerOpen(GravityCompat.START))
                        drawer.closeDrawer(GravityCompat.START);

                    String tempTitle = commonTitle.getText().toString();

                    if (tempTitle.toLowerCase().equals("home") || tempTitle.toLowerCase().equals("title")) {

                        commonActionButton.setText("SAVE");
                        commonActionButton.setTypeface(null);
                        commonTitle.setText("ADD MEMO");

                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.fragment_place, new AddMemoFragment())
                                .addToBackStack(null)
                                .commit();
                    }
                    if (tempTitle.toLowerCase().equals("add memo")) {
                        HomeScreenMethods.onSaveAddedMemoClick(HomeActivity.this, getSupportFragmentManager(), commonTitle, commonActionButton);
                    }
                    if (tempTitle.toLowerCase().equals("profile")) {
                        HomeScreenMethods.onEditOrSaveProfileClick(HomeActivity.this, getSupportFragmentManager(), commonTitle, commonActionButton, drawerName);
                    }
                    if (tempTitle.toLowerCase().equals("change password")) {
                        HomeScreenMethods.onSaveChangedPasswordClick(HomeActivity.this, getSupportFragmentManager(), commonTitle, commonActionButton);
                    }
                    if (tempTitle.toLowerCase().equals("show memo")) {
                        HomeScreenMethods.onEditMemoClick(HomeActivity.this, getSupportFragmentManager(), commonTitle, commonActionButton);
                    }
                    if (tempTitle.toLowerCase().equals("edit memo")) {
                        HomeScreenMethods.onSaveEditedMemoClick(HomeActivity.this, getSupportFragmentManager(), commonTitle, commonActionButton);
                    }
                }
            });

            drawerIcon = (TextView) findViewById(R.id.nav_draw_icon);
            drawerIcon.setText("N");
            drawerIcon.setTypeface(tf1);
            drawerIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (HomeActivity.drawer
                            .isDrawerOpen(GravityCompat.START)) {
                        HomeActivity.drawer.closeDrawer(GravityCompat.START);
                    } else {
                        HomeActivity.drawer.openDrawer(GravityCompat.START);
                    }
                }
            });

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_place, new HomeFragment())
                    .commit();

        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else {
            super.onBackPressed();
            String tempTitle = commonTitle.getText().toString();
            if (tempTitle.toLowerCase().equals("home")) {
                finish();
            } else {
                commonActionButton.setText("A");
                commonActionButton.setTypeface(tf1);
                commonTitle.setText("HOME");

                getFragmentManager().popBackStackImmediate();

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_place, new HomeFragment())
                        .commit();
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        try {
            String tempTitle = null;
            int id = menuItem.getItemId();
            drawer.closeDrawer(GravityCompat.START);

            if (id == R.id.item_logout) {
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }

            if (id == R.id.item_edit_profile) {
                tempTitle = commonTitle.getText().toString();
                commonActionButton.setText("EDIT");
                commonActionButton.setTypeface(null);
                commonTitle.setText("PROFILE");

                if (!tempTitle.toLowerCase().equals("home")) {
                    getSupportFragmentManager().popBackStackImmediate();
                }
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_place, new EditProfileFragment())
                        .addToBackStack(null)
                        .commit();
            }

            if (id == R.id.item_changepassword) {
                tempTitle = commonTitle.getText().toString();
                commonActionButton.setText("SAVE");
                commonActionButton.setTypeface(null);
                commonTitle.setText("CHANGE PASSWORD");

                if (!tempTitle.toLowerCase().equals("home")) {
                    getSupportFragmentManager().popBackStackImmediate();
                }
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_place, new ChangePasswordFragment())
                        .addToBackStack(null)
                        .commit();
            }

            if (id == R.id.item_home) {
                tempTitle = commonTitle.getText().toString();
                commonActionButton.setText("A");
                commonActionButton.setTypeface(tf1);
                commonTitle.setText("HOME");

                if (!tempTitle.toLowerCase().equals("home")) {
                    getSupportFragmentManager().popBackStackImmediate();
                }
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_place, new HomeFragment())
                        .commit();
            }

            if (id == R.id.item_setalarm) {
                tempTitle = commonTitle.getText().toString();
                commonActionButton.setText("SAVE");
                commonActionButton.setTypeface(null);
                commonTitle.setText("ADD MEMO");

                if (!tempTitle.toLowerCase().equals("home")) {
                    getSupportFragmentManager().popBackStackImmediate();
                }
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_place, new AddMemoFragment())
                        .addToBackStack(null)
                        .commit();
            }
        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
        return true;
    }

//    public void onSaveMemo(View view) {
//        try {
//            editAddMemo = (EditText) findViewById(R.id.edt_add_memo_text);
//            imgAddMemo = (ImageView) findViewById(R.id.img_add_memo);
//
//            memoText = editAddMemo.getText().toString();
//
//            BitmapDrawable drawableMemo = (BitmapDrawable) imgAddMemo.getDrawable();
//            Bitmap bitmapMemo = drawableMemo.getBitmap();
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            bitmapMemo.compress(Bitmap.CompressFormat.JPEG, 90, stream);
//            byte[] bytesMemo = stream.toByteArray();
//            Log.v("Image byte array... ", bytesMemo.toString());
//            imageInBase64 = Base64.encodeToString(bytesMemo, Base64.DEFAULT);
//
//            StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                    URLConstants.ADD_MEMO,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            try {
//                                JSONObject responseObject = new JSONObject(response);
//                                responseMessage = responseObject.getString("msg");
//                                responseStatus = responseObject.getString("status");
//                                if (responseStatus.equals(SUCCESS_RESPONSE)) {
//
//                                    Log.v("MemoAdd status...", responseStatus + ". AS ." + responseMessage);
//                                    CustomToastMaker.makeToast(HomeActivity.this, "Memo Added successfully..!");
//
//                                    commonTitle = (TextView) findViewById(R.id.action_bar_title);
//                                    commonActionButton = (TextView) findViewById(R.id.action_bar_button);
//                                    commonActionButton.setText("A");
//                                    commonActionButton.setTypeface(tf1);
//                                    commonTitle.setText("HOME");
//
//                                    getSupportFragmentManager().popBackStackImmediate();
//
//                                } else {
//                                    CustomToastMaker.makeToast(HomeActivity.this, responseMessage);
//                                }
//                            } catch (Exception e) {
//                                Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
//                                e.printStackTrace();
//                            }
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            JsonErrorsCheck.whichErrorInJsonRequest(error);
//                        }
//                    }) {
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    Map<String, String> postParam = new HashMap<>();
//                    postParam.put("rquest", "addmemo");
//                    postParam.put("user_Id", String.valueOf(userId));
//                    postParam.put("text", memoText);
//                    postParam.put("image_Url", imageInBase64);
////                    Log.v("Request ", "...... parameters" + postParam.toString());
//                    return postParam;
//                }
//            };
//
//            AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");
//
//        } catch (Exception e) {
//            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
//            e.printStackTrace();
//        }
//    }

//    public void setEditMemoImage(View view) {
//        AppController.preferences.savePreference("pref_setimage", "edit");
//        Log.v("Img set status...", "Image editing...");
//        // Create intent to Open Image applications like Gallery, Google Photos
//        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
//                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        // Start the Intent
//        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
//    }

//    public void setAddMemoImage(View view) {
//        AppController.preferences.savePreference("pref_setimage", "add");
//        Log.v("Img set status...", "Image adding...");
//        // Create intent to Open Image applications like Gallery, Google Photos
//        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
//                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        // Start the Intent
//        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
//
//    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        try {
//            // When an Image is picked
//            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
//                // Get the Image from data
//
//                Uri selectedImage = data.getData();
//                String[] filePathColumn = {MediaStore.Images.Media.DATA};
//
//                // Get the cursor
//                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
//                // Move to first row
//                cursor.moveToFirst();
//
//                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                imgDecodableString = cursor.getString(columnIndex);
//                System.out.println(imgDecodableString);
//                cursor.close();
//
////-------- Converting the image to Base64 byteArray....----------------------------------------
//
//                Bitmap bitmap = BitmapFactory.decodeFile(imgDecodableString);
//                ByteArrayOutputStream bitmapInBytes = new ByteArrayOutputStream();
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bitmapInBytes);
//                bitmapInBytes.toByteArray();
//
////                byte[] base64Bytes = Base64.encode(bitmapInBytes.toByteArray(), Base64.DEFAULT);
////                String base64String = Base64.encodeToString(bitmapInBytes.toByteArray(), Base64.DEFAULT);
////---------------------------------------------------------------------------------------------
//                String setImageFor = AppController.preferences.getPreference("pref_setimage", null);
//                ImageView imgView = null;
//                if (setImageFor.equals("edit")) {
//                    imgView = (ImageView) findViewById(R.id.img_edit_memo);
//                    imgView.setImageBitmap(bitmap);
//                }
//                // Set the Image in ImageView after decoding the String
////                imgView.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
//                if (setImageFor.equals("add")) {
//                    imgView = (ImageView) findViewById(R.id.img_add_memo);
//                    imgView.setImageBitmap(bitmap);
//                }
//            } else {
//                CustomToastMaker.makeToast(this, "You haven't picked Image");
//            }
//        } catch (Exception e) {
//            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
//            e.printStackTrace();
//        }
//    }

//    public Bitmap stringToBitMap(String encodedString) {
//        try {
//            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
//            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
//            return bitmap;
//        } catch (Exception e) {
//            e.getMessage();
//            return null;
//        }
//    }

    public void onCancel(View view) {
        try {

            getSupportFragmentManager().popBackStackImmediate();
            CustomProgressBar.dismissProgressDialog();

            commonTitle = (TextView) findViewById(R.id.action_bar_title);
            commonActionButton = (TextView) findViewById(R.id.action_bar_button);
            commonActionButton.setText("A");
            commonActionButton.setTypeface(tf1);
            commonTitle.setText("HOME");

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_place, new HomeFragment())
                    .commit();
        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
    }

//    public void onEditProfile(View view) {
//        try {
//
//            Button btnSaveEdit = (Button) findViewById(R.id.btn_edit_save_profile);
//            String btnText = btnSaveEdit.getText().toString();
//
//            EditText nameEditText = (EditText) findViewById(R.id.edit_name);
//
//            commonTitle = (TextView) findViewById(R.id.action_bar_title);
//            commonActionButton = (TextView) findViewById(R.id.action_bar_button);
//
//            if (btnText.toLowerCase().equals("edit")) {
//                CustomToastMaker.makeToast(HomeActivity.this, "You can edit the name only...!");
//                nameEditText.setEnabled(true);
//                btnSaveEdit.setText("SAVE");
//                commonActionButton.setText("SAVE");
//                commonActionButton.setTypeface(null);
//            }
//
//            if (btnText.toLowerCase().equals("save")) {
//                String errorMessage = "Please enter valid";
//                userName = nameEditText.getText().toString();
//                if (userName.trim().equals("")) {
//                    errorMessage += " User name";
//                }
//                if (!errorMessage.equals("Please enter valid")) {
//                    CustomToastMaker.makeToast(HomeActivity.this, errorMessage);
//                    return;
//                }
//
//                String uri = String.format(URLConstants.EDIT_PROFILE + "?rquest=%1$s&user_Id=%2$s&newname=%3$s",
//                        "myprofileupdate_details",
//                        String.valueOf(userId),
//                        userName);
//
//                StringRequest stringRequest = new StringRequest(Request.Method.GET,
//                        uri,
//                        new Response.Listener<String>() {
//                            @Override
//                            public void onResponse(String response) {
//                                try {
//                                    JSONObject responseObject = new JSONObject(response);
//                                    responseStatus = responseObject.getString("status");
//                                    responseMessage = responseObject.getString("msg");
//
//                                    if (responseStatus.equals(SUCCESS_RESPONSE)) {
//                                        CustomToastMaker.makeToast(HomeActivity.this, "Profile updated successfully..!");
//                                        AppController.preferences.savePreference("pref_name", userName);
//
//                                        drawerName = (TextView) findViewById(R.id.txt_header_name);
//                                        drawerName.setText(AppController.preferences.getPreference("pref_name", "No name"));
//
//                                        commonTitle = (TextView) findViewById(R.id.action_bar_title);
//                                        commonActionButton = (TextView) findViewById(R.id.action_bar_button);
//                                        commonActionButton.setText("A");
//                                        commonActionButton.setTypeface(tf1);
//                                        commonTitle.setText("HOME");
//
//                                        getSupportFragmentManager().popBackStackImmediate();
//
//                                    } else {
//                                        CustomToastMaker.makeToast(HomeActivity.this, responseMessage);
//                                    }
//                                } catch (Exception e) {
//                                    Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
//                                    e.printStackTrace();
//                                }
//                            }
//                        },
//                        new Response.ErrorListener() {
//                            @Override
//                            public void onErrorResponse(VolleyError error) {
//                                JsonErrorsCheck.whichErrorInJsonRequest(error);
//                            }
//                        });
//
//                AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");
////                CustomToastMaker.makeToast(HomeActivity.this, "Profile updated successfully...!");
////                commonActionButton.setText("A");
////                commonActionButton.setTypeface(tf1);
////                commonTitle.setText("HOME");
////
////                getSupportFragmentManager()
////                        .beginTransaction()
////                        .replace(R.id.fragment_place, new HomeFragment())
////                        .commit();
//            }
//        } catch (Exception e) {
//            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
//            e.printStackTrace();
//        }
//    }

//    public void onSaveChangedPassword(View view) {
//        try {
//            String errorMessage = "Please enter valid";
//            editOldPassword = (EditText) findViewById(R.id.edit_old_password);
//            oldPassword = editOldPassword.getText().toString();
//
//            editNewPassword = (EditText) findViewById(R.id.edit_new_password);
//            newPassword = editNewPassword.getText().toString();
//
//            editConfirmNewPassword = (EditText) findViewById(R.id.edit_confirm_new_password);
//            confirmNewPassword = editConfirmNewPassword.getText().toString();
//
//            if (oldPassword.equals("") || oldPassword.length() < 6 || oldPassword.length() > 25) {
//                errorMessage += " Old Password";
//            }
//            if (newPassword.equals("") || newPassword.length() < 6 || newPassword.length() > 25) {
//                errorMessage += " New Password (6-25 chars)";
//            }
//            if (confirmNewPassword.equals("") || confirmNewPassword.length() < 6 || confirmNewPassword.length() > 25) {
//                errorMessage += " Confirm New Password (6-25 chars)";
//            }
//
//            if (!errorMessage.equals("Please enter valid")) {
//                CustomToastMaker.makeToast(HomeActivity.this, errorMessage);
//                return;
//            }
//
//            if (!newPassword.equals(confirmNewPassword)) {
//                CustomToastMaker.makeToast(HomeActivity.this, "New password and confirm password should be same, please check");
//                return;
//            }
//
//            StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                    URLConstants.CHANGE_PASSWORD,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            try {
//                                JSONObject responseObject = new JSONObject(response);
//                                responseStatus = responseObject.getString("status");
//                                responseMessage = responseObject.getString("msg");
//
//                                if (responseStatus.equals(SUCCESS_RESPONSE)) {
//                                    CustomToastMaker.makeToast(HomeActivity.this, "Password changed successfully..!");
//                                    commonTitle = (TextView) findViewById(R.id.action_bar_title);
//                                    commonActionButton = (TextView) findViewById(R.id.action_bar_button);
//                                    commonActionButton.setText("A");
//                                    commonActionButton.setTypeface(tf1);
//                                    commonTitle.setText("HOME");
//
//                                    getSupportFragmentManager().popBackStackImmediate();
//
//                                } else {
//                                    CustomToastMaker.makeToast(HomeActivity.this, responseMessage);
//                                }
//                            } catch (Exception e) {
//                                Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
//                                e.printStackTrace();
//                            }
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            JsonErrorsCheck.whichErrorInJsonRequest(error);
//                        }
//                    }) {
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    Map<String, String> postParam = new HashMap<>();
//                    postParam.put("rquest", "changepass");
//                    postParam.put("user_Id", String.valueOf(userId));
//                    postParam.put("password", oldPassword);
//                    postParam.put("newpassword", newPassword);
//                    return postParam;
//                }
//            };
//
//            AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");
//
//        } catch (Exception e) {
//            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
//            e.printStackTrace();
//        }
//    }

//    public void onEditMemo(View view) {
//        try {
//            commonTitle = (TextView) findViewById(R.id.action_bar_title);
//            commonActionButton = (TextView) findViewById(R.id.action_bar_button);
//            commonActionButton.setText("SAVE");
//            commonActionButton.setTypeface(null);
//            commonTitle.setText("EDIT MEMO");
//
//            getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(R.id.fragment_place, new EditMemoFragment())
//                    .addToBackStack(null)
//                    .commit();
//
//        } catch (Exception e) {
//            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
//            e.printStackTrace();
//        }
//    }

//    public void onSaveEditedMemo(View view) {
//        try {
//            edtEditMemo = (EditText) findViewById(R.id.edt_edit_memo_text);
//            imgEditMemo = (ImageView) findViewById(R.id.img_edit_memo);
//
//            memoText = edtEditMemo.getText().toString();
//
//            BitmapDrawable drawableMemo = (BitmapDrawable) imgEditMemo.getDrawable();
//            Bitmap bitmapMemo = drawableMemo.getBitmap();
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            bitmapMemo.compress(Bitmap.CompressFormat.JPEG, 90, stream);
//            byte[] bytesMemo = stream.toByteArray();
//            Log.v("Image byte array... ", bytesMemo.toString());
//            imageInBase64 = Base64.encodeToString(bytesMemo, Base64.DEFAULT);
//
//            StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                    URLConstants.EDIT_MEMO,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            try {
//                                JSONObject responseObject = new JSONObject(response);
//                                responseMessage = responseObject.getString("msg");
//                                responseStatus = responseObject.getString("status");
//                                if (responseStatus.equals(SUCCESS_RESPONSE)) {
//
//                                    Log.v("MemoEdit status...", responseStatus + ". AS ." + responseMessage);
//                                    CustomToastMaker.makeToast(HomeActivity.this, "Memo Edited successfully..!");
//
//                                    AppController.preferences.savePreference("pref_memotext", memoText);
////                                    AppController.preferences.savePreference("pref_memoimageurl", selectedMemo.getMemoImageUrl());
//
//                                    getSupportFragmentManager().popBackStackImmediate();
//                                    getSupportFragmentManager().popBackStackImmediate();
//
//                                    commonTitle = (TextView) findViewById(R.id.action_bar_title);
//                                    commonActionButton = (TextView) findViewById(R.id.action_bar_button);
//                                    commonActionButton.setText("A");
//                                    commonActionButton.setTypeface(tf1);
//                                    commonTitle.setText("HOME");
//
//                                } else {
//                                    CustomToastMaker.makeToast(HomeActivity.this, responseMessage);
//                                }
//                            } catch (Exception e) {
//                                Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
//                                e.printStackTrace();
//                            }
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            JsonErrorsCheck.whichErrorInJsonRequest(error);
//                        }
//                    }) {
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    Map<String, String> postParam = new HashMap<>();
//                    postParam.put("rquest", "editmemo");
//                    postParam.put("user_Id", String.valueOf(userId));
//                    postParam.put("content_Id", AppController.preferences.getPreference("pref_contentid", "invalid"));
//                    postParam.put("image_Url", imageInBase64);
//                    postParam.put("text", memoText);
//                    return postParam;
//                }
//            };
//
//            AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");
//
//        } catch (Exception e) {
//            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
//            e.printStackTrace();
//        }
//    }
//
//    public void onRemoveMemo(View view) {
//        try {
//
//            String uri = String.format(URLConstants.REMOVE_MEMO + "?rquest=%1$s&user_Id=%2$s&content_Id=%3$s",
//                    "removememo",
//                    userId,
//                    AppController.preferences.getPreference("pref_contentid", "invalid"));
//
//            StringRequest stringRequest = new StringRequest(Request.Method.GET,
//                    uri,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            try {
//                                JSONObject responseObject = new JSONObject(response);
//
//                                Log.v("Remove Response... ", responseObject.toString());
//
//                                responseStatus = responseObject.getString("status");
//                                responseMessage = responseObject.getString("msg");
//
//                                if (responseStatus.equals(SUCCESS_RESPONSE)) {
//
//                                    getSupportFragmentManager().popBackStackImmediate();
//                                    getSupportFragmentManager().popBackStackImmediate();
//
//                                    commonTitle = (TextView) findViewById(R.id.action_bar_title);
//                                    commonActionButton = (TextView) findViewById(R.id.action_bar_button);
//                                    commonActionButton.setText("A");
//                                    commonActionButton.setTypeface(tf1);
//                                    commonTitle.setText("HOME");
//
//                                    Log.e("Remove status...", "The memo has been removed...!");
//
//                                }
//
//                                Log.v("Remove status...", responseMessage);
//                                CustomToastMaker.makeToast(HomeActivity.this, "Remove status..." + responseMessage);
//                            } catch (JSONException e) {
//                                Log.e("JSONException...", "You got json exception on remove response");
//                                e.printStackTrace();
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            JsonErrorsCheck.whichErrorInJsonRequest(error);
//                        }
//                    });
//            AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");
//
//        } catch (Exception e) {
//            Log.e("Exception : ", "Exception in class " + ClassNames.HOME_ACTIVITY + " :: " + e.getMessage());
//            e.printStackTrace();
//        }
//    }

}
