package com.mycompany.mymemo.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mycompany.mymemo.R;
import com.mycompany.mymemo.app.AppController;
import com.mycompany.mymemo.clickevents.LoginScreenMethods;
import com.mycompany.mymemo.components.CustomProgressBar;
import com.mycompany.mymemo.components.CustomToastMaker;
import com.mycompany.mymemo.constants.ClassNames;
import com.mycompany.mymemo.constants.URLConstants;
import com.mycompany.mymemo.fragments.ForgetPasswordFragment;
import com.mycompany.mymemo.fragments.LoginFragment;
import com.mycompany.mymemo.json.JsonErrorsCheck;
import com.mycompany.mymemo.localapi.LocalValidators;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

//    private String SUCCESS_RESPONSE = "Success";
//
//    private String emailId = "", password = "", responseStatus, responseMessage, userName, userEmail, userType;
//    private int userId;

    private Toolbar toolbar;
    private Typeface tfaceBackButton;

    private TextView commonTitle, commonActionButton, drawerIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        try {

            toolbar = (Toolbar) findViewById(R.id.mycustom_toolbar);
            setSupportActionBar(toolbar);

            tfaceBackButton = Typeface.createFromAsset(getAssets(), "back_button_icon.ttf");
            commonActionButton = (TextView) findViewById(R.id.action_bar_button);
            commonTitle = (TextView) findViewById(R.id.action_bar_title);

            commonTitle.setText("LOGIN");
            drawerIcon = (TextView) findViewById(R.id.nav_draw_icon);
            drawerIcon.setText("B");
            drawerIcon.setTypeface(tfaceBackButton);
            drawerIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String tempTitle = commonTitle.getText().toString();
                    if (tempTitle.toLowerCase().equals("login") || tempTitle.toLowerCase().equals("title"))
                        finish();
                    else {
                        commonTitle.setText("LOGIN");

//                        commonActionButton = (TextView) findViewById(R.id.action_bar_button);
                        commonActionButton.setText("LOGIN");

                        getSupportFragmentManager().popBackStackImmediate();
                    }
                }
            });


            commonActionButton.setText("LOGIN");
            commonActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String tempTitle = commonTitle.getText().toString();
                    if (tempTitle.toLowerCase().equals("login") || tempTitle.toLowerCase().equals("title"))
                        LoginScreenMethods.onLoginClick(LoginActivity.this);
                }
            });

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_place, new LoginFragment())
                    .commit();

        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.LOGIN_ACTIVITY);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        String tempTitle = commonTitle.getText().toString();

        if (tempTitle.toLowerCase().equals("login") || tempTitle.toLowerCase().equals("title")) {
            finish();
        } else {
            commonActionButton.setText("LOGIN");
            commonTitle.setText("LOGIN");
//            getFragmentManager().popBackStackImmediate();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_place, new LoginFragment())
                    .commit();
        }
    }

    public void onLogin(View view) {
//        try {
//
//            CustomProgressBar.showProgressDialog(this, "Please wait...", false);
//
//            String errorMessage = "Please enter valid";
//
//            EditText emailEditText = (EditText) findViewById(R.id.edit_email_login);
//            emailId = emailEditText.getText().toString();
//
//            if (emailId.equals("") || !LocalValidators.isValidEmail(emailId)) {
//                errorMessage += " Email-id";
//            }
//
//            EditText passwordEditText = (EditText) findViewById(R.id.edit_password);
//            password = passwordEditText.getText().toString();
//
//            if (password.equals("") || password.length() < 6 || password.length() > 25) {
//                errorMessage += " Password";
//            }
//
//            if (!errorMessage.equals("Please enter valid")) {
//                CustomToastMaker.makeToast(LoginActivity.this, errorMessage);
//                CustomProgressBar.dismissProgressDialog();
//                return;
//            }
//
//
//// ------------------- string request -------------------
//            StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                    URLConstants.LOGIN,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            try {
//                                Log.v("Response.....:", response);
//
//                                JSONObject responseObject = new JSONObject(response);
//                                responseStatus = responseObject.getString("status");
//                                responseMessage = responseObject.getString("msg");
//
//                                if (responseStatus.equals(SUCCESS_RESPONSE)) {
//                                    JSONObject userDetails = responseObject.getJSONObject("Login");
//                                    userId = userDetails.getInt("User_id");
//                                    userName = userDetails.getString("Name");
//                                    userEmail = userDetails.getString("Email");
//                                    userType = userDetails.getString("User_type");
//
//                                    AppController.preferences.savePreference("pref_email", userEmail);
//                                    AppController.preferences.savePreference("pref_name", userName);
//                                    AppController.preferences.savePreference("pref_usertype", userType);
//                                    AppController.preferences.savePreference_Int("pref_userid", userId);
//
//                                    JSONArray memoList = responseObject.getJSONArray("Memo List");
//                                    Log.v("Id.....", "" + userId);
//                                    Log.v("Name...", userName);
//                                    Log.v("Email..", userEmail);
//                                    Log.v("Type....", userType);
//                                    Log.v("List...", memoList.toString());
//                                    if (memoList.length() <= 0) {
//                                        AppController.preferences.savePreference("pref_memoListString", "-");
//                                    } else {
//                                        AppController.preferences.savePreference("pref_memoListString", memoList.toString());
//                                    }
//                                    CustomProgressBar.dismissProgressDialog();
//
//                                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
//                                    startActivity(intent);
//
//                                } else {
//                                    CustomToastMaker.makeToast(LoginActivity.this, responseMessage);
//                                    CustomProgressBar.dismissProgressDialog();
//                                }
//                            } catch (Exception e) {
//                                CustomToastMaker.makeToast(LoginActivity.this, " exception in response...!");
//                                e.printStackTrace();
//                            } finally {
//                                CustomProgressBar.dismissProgressDialog();
//                            }
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            try {
//                                JsonErrorsCheck.whichErrorInJsonRequest(error);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }) {
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    Map<String, String> postParam = new HashMap<>();
//                    postParam.put("rquest", "login");
//                    postParam.put("email", emailId);
//                    postParam.put("password", password);
//                    Log.v("Request ", "...... parameters");
//                    return postParam;
//                }
//            };
//            Log.v("Response", "...... Before service call");
//            AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");
//            Log.v("Response", "...... After service call");
//        } catch (Exception e) {
//            Log.e("Exception : ", "Exception in class " + ClassNames.LOGIN_ACTIVITY + " :: " + e.getMessage());
//            e.printStackTrace();
//        } finally {
//            CustomProgressBar.dismissProgressDialog();
//        }
    }

    public void onForgetPassword(View view) {
//        try {
////            commonTitle = (TextView) findViewById(R.id.action_bar_title);
//            commonTitle.setText("FORGOT PASSWORD");
//
////            commonActionButton = (TextView) findViewById(R.id.action_bar_button);
//            commonActionButton.setText("");
//
//            getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(R.id.fragment_place, new ForgetPasswordFragment())
//                    .addToBackStack(null)
//                    .commit();
//        } catch (Exception e) {
//            Log.e("Exception : ", "Exception in class " + ClassNames.LOGIN_ACTIVITY + " :: " + e.getMessage());
//            e.printStackTrace();
//        }
    }

    public void onPasswordReset(View view) {
//        try {
//
//            EditText editText_email = (EditText) findViewById(R.id.edit_email_forget);
//            emailId = editText_email.getText().toString();
//
//            if (!LocalValidators.isValidEmail(emailId)) {
//                CustomToastMaker.makeToast(LoginActivity.this, "Please enter valid email Id...!");
//                return;
//            }
//
//            StringRequest stringRequest = new StringRequest(Request.Method.POST, URLConstants.FORGET_PASSWORD,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            try {
//                                JSONObject jsonObject = new JSONObject(response);
//                                responseStatus = jsonObject.getString("status");
//                                responseMessage = jsonObject.getString("msg");
//
//                                if (responseStatus.equals("Success")) {
//                                    CustomToastMaker.makeToast(LoginActivity.this, "New password sent to your mail id...!");
//
////                                    commonTitle = (TextView) findViewById(R.id.action_bar_title);
//                                    commonTitle.setText("LOGIN");
//
////                                    commonActionButton = (TextView) findViewById(R.id.action_bar_button);
//                                    commonActionButton.setText("LOGIN");
//
//                                    getSupportFragmentManager().popBackStackImmediate();
//
//                                    getSupportFragmentManager()
//                                            .beginTransaction()
//                                            .replace(R.id.fragment_place, new LoginFragment())
//                                            .commit();
//
//                                } else {
//                                    CustomToastMaker.makeToast(LoginActivity.this, responseMessage);
//                                    return;
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            try {
//                                JsonErrorsCheck.whichErrorInJsonRequest(error);
//                            } catch (Exception e) {
//                                Log.e("Exception : ", "Exception in class " + ClassNames.LOGIN_ACTIVITY + " :: " + e.getMessage());
//                                e.printStackTrace();
//                            }
//                        }
//                    }) {
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    Map<String, String> postParam = new HashMap<>();
//
//                    postParam.put("rquest", "resetpass");
//                    postParam.put("email", emailId);
//
//                    Log.v("Params for frgPass : ", postParam.toString());
//
//                    return postParam;
//                }
//            };
//
//            AppController.getInstance().addToRequestQueue(stringRequest, "json_string_request");
//
//        } catch (Exception e) {
//            Log.e("Exception : ", "Exception in class " + ClassNames.LOGIN_ACTIVITY + " :: " + e.getMessage());
//            e.printStackTrace();
//        }
    }

}
