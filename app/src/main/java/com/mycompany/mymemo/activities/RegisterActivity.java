package com.mycompany.mymemo.activities;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.mycompany.mymemo.R;
import com.mycompany.mymemo.components.CustomToastMaker;
import com.mycompany.mymemo.constants.ClassNames;

import java.util.ArrayList;
import java.util.List;

public class RegisterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private String emailId = "", password = "", confirmPassword = "", name = "", userType = "None", mobileNo = "",
            responseStatus, responseMessage;

    private Toolbar toolbar;
    private Typeface tfaceBackButton;

    private TextView commonTitle, commonActionButton, drawerIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        try {

            toolbar = (Toolbar) findViewById(R.id.mycustom_toolbar);
            setSupportActionBar(toolbar);

            tfaceBackButton = Typeface.createFromAsset(getAssets(),"back_button_icon.ttf");
            commonTitle = (TextView) findViewById(R.id.action_bar_title);
            commonTitle.setText("REGISTER");
            drawerIcon = (TextView) findViewById(R.id.nav_draw_icon);
            drawerIcon.setText("B");
            drawerIcon.setTypeface(tfaceBackButton);
            drawerIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            commonActionButton = (TextView) findViewById(R.id.action_bar_button);
            commonActionButton.setText("SUBMIT");
            commonActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRegister(v);
                }
            });


            Spinner spinner = (Spinner) findViewById(R.id.spinner_user_type);
            spinner.setOnItemSelectedListener(this);

            List<String> userType = new ArrayList<>();

            userType.add("Student");
            userType.add("Professional");
            userType.add("Musician");

            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, userType);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);

        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.REGISTER_ACTIVITY);
        }
    }

    // ------------------- methods for menu items --------------
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = new MenuInflater(RegisterActivity.this);
//        inflater.inflate(R.menu.submit, menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
////        onRegister();
//        return super.onOptionsItemSelected(item);
//    }

    // --------------- methods for spinner -----------------
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        userType = (String) parent.getItemAtPosition(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void onRegister(View view) {
        try {

        } catch (Exception e) {
            Log.e("Exception : ", "Exception in class " + ClassNames.REGISTER_ACTIVITY + " :: " + e.getMessage());
            e.printStackTrace();
        }
        CustomToastMaker.makeToast(this,"You are registered successfully...!");
    }
}
